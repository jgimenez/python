# !/usr/bin/python
# -*-coding: utf-8-*-
##############################################
#          Llegim la taula "clientes"        #
##############################################

import os
import sys

'''
CAl fer "dnf -y install python3-psycopg2"
Editem l’arxiu pg_hba.conf (nano /var/lib/pgsql/data/pg_hba.conf)
sys
Comentem la línia:
 #local   all             all                                     peer

Afegim la línea:
local   all             all                                     trust
'''
import psycopg2


##############################################
#          Ens connectem a la BBDD           #
##############################################
try:
	conn = psycopg2.connect(database="training", user="postgres", password="jupiter")
	print("DATABASE OPENED SUCCESSFULLY \n")
	
except:
	print("CONNECTION ERROR")
	exit(2)



##############################################
#            Declarem el cursor              #
##############################################
cur = conn.cursor()

os.system('clear')
sortir = False

##############################################
#              Menu principal                #
##############################################
while sortir == False:	
	
	os.system('clear')
	print("OPCIONS \n 1- Llegir taula 'clientes': \n 0- Sortir \n")

	opcio = input('Escull una opció [0-1]: ')
	
    # Comprovem si l'opció és correcta
	if not opcio.isdigit() or not ( int(opcio) >= 0 and int(opcio) <= 1 ):
		os.system('clear')
		print("Opció incorrecta, torna a provar \n")
		tecla = input('Prem una tecla per continuar')

	else:
		opcio = int(opcio)

    # Sortim
	if opcio == 0:
		print("Adeu! \n")
		sortir = True

	# Llegim la taula "clientes"
	elif opcio == 1: 
		
		try:			
				cur.execute("SELECT * FROM clientes");
				rows = cur.fetchall()

				for row in rows:
				   print("\n")
				   print("NUM_CLIE       = ", row[0])
				   print("EMPRESA        = ", row[1])
				   print("REP_CLIE       = ", row[2])
				   print("LIMITE_CREDITO = ", row[3])
				   print("\n")
				
				tecla = input('Prem una tecla per continuar')
		
		except psycopg2.Error as er :
			print("-------- ERROR:", er.pgcode, " -------- \n")
			conn.rollback()

##############################################
#        Ens desconnectem de la BBDD         #
##############################################
conn.close()
