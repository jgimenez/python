# !/usr/bin/python
# -*-coding: utf-8-*-
# Requiere tkinter instalado:   
#     Fedora: dnf install python3-tkinter
#     Debian: apt install python3-tk

########################################################################
#                               IMPORT
########################################################################
from tkinter import *
from tkinter import ttk
from tkinter import messagebox

##############################################
#          Llegim la taula "clientes"        #
##############################################

import os

'''
Cal fer
     Fedora: dnf -y install python3-psycopg2
     Debian: apt install python3-psycopg2
Editem l’arxiu pg_hba.conf
     Fedora: nano /var/lib/pgsql/data/pg_hba.conf
     Debian: nano/etc/postgresql/9.6/main/pg_hba.conf
sys
Comentem la línia:
 #local   all             all                                     peer

Afegim la línea:
local   all             all                                     trust
'''
import psycopg2

#######################  FUNCIONES DESDE TKINTER  ######################
def ejecutar_sql(event=None):
    try:
            sql="SELECT * FROM " + nombre_tabla_entry.get()
            cur.execute(sql);
            rows = cur.fetchall()
            tot_rows = rows.__len__()
            columns = rows[0].__len__()

            tree.delete(*tree.get_children())

            # Formateamos las columnas del tree
            list_columns = [columnames[0] for columnames in cur.description]
            tree['columns'] = list_columns

            for column in list_columns:
                tree.column(column, width=100)
                tree.heading(column, text=column.capitalize())


            for row in rows:
                tree.insert("", 0, text="", values=row)

            # Mostramos los elementos de la ventana
            tree.pack(side = 'left', expand = 1, fill = 'both')  # Hacemos que el tree ocupe toda la ventana

    except psycopg2.Error as er :
        print("-------- ERROR:", er.pgcode, " -------- \n")
        messagebox.showerror("Error", "Se ha producido un error al acceder a la BD. Código del error: " + er.pgcode)
        conn.rollback()


def DobleClick(event):
    item = tree.selection()
    for i in item:
        # Tots els valors
        missatge="Ha seleccionat:" + str(tree.item(i, "values"))
        print(missatge)
        messagebox.showinfo("Tots els valors",missatge)

        # columna0 i columna1
        missatge="Ha seleccionat:" + str(tree.item(i, "values")[0] + "," + tree.item(i, "values")[1])
        print(missatge)
        messagebox.showinfo("columna0 i columna1",missatge)
###############################  MAIN  #################################


##############################################
#          Ens connectem a la BBDD           #
##############################################
try:
    conn = psycopg2.connect(database="training", user="postgres", password="jupiter")
    print("DATABASE OPENED SUCCESSFULLY \n")

except:
    print("CONNECTION ERROR")
    exit(2)



##############################################
#            Declarem el cursor              #
##############################################
cur = conn.cursor()
os.system('clear')

##############################################
#              Menu principal                #
##############################################

##############################################
# Definimos la ventana
##############################################
ventana = Tk()
ventana.title("Visualizador de tablas")
ventana.geometry('900x400')
# No permitimos el resize de la ventana
ventana.resizable(width=0, height=0)

##############################################
# Definimos los elementos de la ventana
##############################################
# El Tree que contendrá la tabla sql
tree = ttk.Treeview(ventana)
# La barra de scroll vertical
vertical_scroll_bar = ttk.Scrollbar(ventana, orient="vertical", command=tree.yview)
vertical_scroll_bar.pack(side='right', fill='y')
tree.configure(yscrollcommand=vertical_scroll_bar.set)
frame_consulta = ttk.Frame(ventana)   # Frame que contiene Label, Entry y Button
nombre_tabla_label = ttk.Label(frame_consulta, text="Escriba el nombre de la tabla:")
nombre_tabla_entry= ttk.Entry(frame_consulta, width=40)
nombre_tabla_entry.insert("", "")
boton_ejecutar_sql = ttk.Button( frame_consulta, text="Ejecutar sql",
                                 command=lambda: ejecutar_sql() )
# Formateamos las columnas del tree
tree['show'] = 'headings'   # Para no mostrar la 1a columna del ID


##############################################
# Mostramos los elementos de la ventana
##############################################
frame_consulta.pack(side=TOP)
nombre_tabla_label.pack(side=LEFT)
nombre_tabla_entry.pack(side=LEFT)
nombre_tabla_entry.focus()   # Ponemos el foco en la caja para escribir texto
boton_ejecutar_sql.pack(side=LEFT)
tree.pack(side = 'left', expand = 1, fill = 'both') # Hacemos que el tree ocupe toda la ventana

##############################################
# Bucle de la ventana
##############################################
tree.bind("<Double-1>", DobleClick)
ventana.bind('<Return>', ejecutar_sql)   # Asociamos la tecla INTRO a ejecutar sql (igual que el botón)
ventana.bind('<KP_Enter>', ejecutar_sql)   # Asociamos la tecla INTRO del teclado numérico a ejecutar sql (igual que el botón)
ventana.mainloop()


##############################################
#        Ens desconnectem de la BBDD         #
##############################################
conn.close()
