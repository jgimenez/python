# !/usr/bin/python
# -*-coding: utf-8-*-
# Requiere tkinter instalado:   # dnf install python3-tkinter

########################################################################
#                               IMPORT
########################################################################
from tkinter import *
from tkinter import ttk

##############################################
#          Llegim la taula "clientes"        #
##############################################

import os

'''
CAl fer "dnf -y install python3-psycopg2"
Editem l’arxiu pg_hba.conf (nano /var/lib/pgsql/data/pg_hba.conf)
sys
Comentem la línia:
 #local   all             all                                     peer

Afegim la línea:
local   all             all                                     trust
'''
import psycopg2


##############################################
#          Ens connectem a la BBDD           #
##############################################
try:
	conn = psycopg2.connect(database="training", user="postgres", password="jupiter")
	print("DATABASE OPENED SUCCESSFULLY \n")
	
except:
	print("CONNECTION ERROR")
	exit(2)



##############################################
#            Declarem el cursor              #
##############################################
cur = conn.cursor()

os.system('clear')

##############################################
#              Menu principal                #
##############################################
# Definimos la ventana
ventana = Tk()
ventana.title("Tabla clientes")
ventana.geometry('800x400')



left_frame = ttk.Frame(ventana, height = 8000, width = 400)
right_frame = ttk.Frame(ventana, height = 5000, width = 400)
left_top = ttk.Frame(left_frame, height = 300, width = 400)
left_bottom = ttk.Frame(left_frame, height = 800)

left_frame.grid(row = 0, column = 0, sticky = 'ns')
right_frame.grid(row = 0, column = 1, sticky = 'ns')
left_top.grid(row = 0, column = 0, sticky = 'ew')
left_bottom.grid(row = 1, column = 0, sticky = 'sew')
left_frame.rowconfigure(1, weight = 1)



# Definimos los elementos de la ventana
#tree = ttk.Treeview(ventana)
tree = ttk.Treeview(left_bottom, selectmode = 'browse')

# Formateamos las columnas del tree
tree['show'] = 'headings'   # Para no mostrar la 1a columna del ID
tree["columns"] = ("c1", "c2", "c3", "c4")

tree.heading("c1", text="NUM_CLIE")
tree.heading("c2", text="EMPRESA")
tree.heading("c3", text="REP_CLIE")
tree.heading("c4", text="LIMITE_CREDITO")

# Añadimos la barra de scroll vertical
left_frame = ttk.Frame(ventana, height = 8000, width = 400)
left_bottom = ttk.Frame(left_frame, height = 800)
scrollbar = ttk.Scrollbar(left_bottom, orient = "vertical", command = tree.yview)
tree.configure(yscrollcommand = scrollbar.set)

try:
        cur.execute("SELECT * FROM clientes");
        rows = cur.fetchall()
        

        for row in rows:
            tree.insert("", 0, text="", values=(row[0], row[1], row[2], row[3]) ),

        # Mostramos los elementos de la ventana        
        tree.pack()
        #tree.pack(side = 'left', expand = 1, fill = 'both') # Hacemos que el tree ocupe toda la ventana
        scrollbar.pack(side = 'left', expand = 1, fill = 'y')

        # Bucle de la ventana
        ventana.mainloop()

except psycopg2.Error as er :
    print("-------- ERROR:", er.pgcode, " -------- \n")
    conn.rollback()

##############################################
#        Ens desconnectem de la BBDD         #
##############################################
conn.close()
