import tkinter as tk
import tkinter.ttk as ttk


def OnDoubleClick(event):
    item = tree.identify('item',event.x,event.y)
    print("you clicked on", tree.item(item,"text"))
    print("you clicked on", tree.item(item,"values"))

if __name__ == "__main__":
    root = tk.Tk()
    tree = ttk.Treeview()
    tree.pack()
    for i in range(10):
        tree.insert("", "end", text="Item %s" % i, values="Item %s" % i)
        #tree.insert("", 0, text="", values=row)
    tree.bind("<Double-1>", OnDoubleClick)
    root.mainloop()
