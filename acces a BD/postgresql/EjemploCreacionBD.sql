SET statement_timeout= 0;
SET client_encoding= 'UTF8';
SET standard_conforming_strings= off;
SET check_function_bodies= false;
SET client_min_messages= warning;
SET escape_string_warning= off;
SET search_path= public, pg_catalog;
SET default_tablespace= '';
SET default_with_oids= false;

-- Previament hem creat la BD
-- CREATE DATABASE botiga;

-- Descarregar aquest arxiu a /tmp
-- # psql botiga
-- botiga=# \i /tmp/EjemploCreacionBD.sql


DROP TABLE IF EXISTS clients;
DROP TABLE IF EXISTS empleats;



CREATE TABLE empleats (
    id_empleat SERIAL, --Clau primaria--
    nom CHARACTER(20) NOT NULL,
    numero_telefon INTEGER,
    PRIMARY KEY (id_empleat)
);

ALTER TABLE public.empleats OWNER TO postgres;




CREATE TABLE clients (
    id_client SERIAL, --Clau primaria--
    id_empleat INTEGER NOT NULL, --Clau forania--
    nom_client CHARACTER(20) NOT NULL,
    cognom_client CHARACTER(20) NOT NULL,
    naixement_client DATE NOT NULL,
    primary key (id_client),
    foreign key (id_empleat) REFERENCES empleats(id_empleat)
);

ALTER TABLE public.clients OWNER TO postgres;





INSERT INTO empleats (nom,numero_telefon) VALUES
    ('Adria',697589665),
    ('Noelia',658883365),
    ('Pol',776852555),
    ('Emily',611211233),
    ('Alex',677852658),
    ('Maria',677852658),
    ('Joel',698569875)
;


INSERT INTO clients (id_empleat,nom_client,cognom_client,naixement_client) VALUES
    (1,'Rosa','Martinez','1990-05-12'),
    (4,'Arnau','Lloveras','1995-04-11'),
    (2,'Eric','Sanchez','1969-11-01'),
    (3,'Pau','Casals','1979-08-29'),
    (6,'Cristina','Gomez','2000-10-03'),
    (7,'Samuel','Ruiz','2001-09-05'),
    (5,'Joni','Gimenez','1990-08-19')
;
