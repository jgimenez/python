#coding: utf-8

from tkinter import *
from tkinter import ttk
num=0



#Funciones
def submitDatos():
	try:	
		tipoNumero1=lista_desplegable1.get()
		tipoNumero2=lista_desplegable2.get()
		numData=str(numEntrada.get())
		num=(numData)
		salidaNumero.delete(0, END)
		



		if tipoNumero1=="decimal":
			while(True):
				for i in num:
					if(i=="0" or i=="1" or i=="2" or i=="3" or i=="4"
						or i=="5" or i=="6" or i=="7" or i=="8" or i=="9"):
						check=True
					elif(i==","):
						check=False
					else:
						check=False

				if check==False:
					# print en salida valor incorrecto
					textoEntradaBin.place(x=1000, y=213)
					textoEntradaHex.place(x=1000, y=213)
					textoEntradaOct.place(x=1000, y=213)
					textoEntradaDec.place(x=25, y=212)
					entradaNumero.delete(0, END)
					break;
				if check==True:
					textoEntradaBin.place(x=1000, y=213)
					textoEntradaOct.place(x=1000, y=213)
					textoEntradaHex.place(x=1000, y=213)
					textoEntradaDec.place(x=1000, y=212)
					break;
			num=int(numData)
			numDec=int(num)
			entradaNumero.delete(0, END)





		elif tipoNumero1=="binaria": #binario
			while(True):
				for i in num:
					if(i=="1" or i=="0"):
						check=True
					else:
						check=False 

				if check==False:
					# print en salida valor incorrecto
					textoEntradaOct.place(x=1000, y=213)
					textoEntradaDec.place(x=1000, y=212)
					textoEntradaHex.place(x=1000, y=213)
					textoEntradaBin.place(x=25, y=213)
					entradaNumero.delete(0, END)
					break;
				elif check==True:
					textoEntradaBin.place(x=1000, y=213)
					textoEntradaOct.place(x=1000, y=213)
					textoEntradaHex.place(x=1000, y=213)
					textoEntradaDec.place(x=1000, y=212)
					break;
			binToDec=int(num, base=2) #num binario a decimal
			numDec=int(binToDec)
			entradaNumero.delete(0, END)


			



		elif tipoNumero1=="octal": #octal
			while(True):
				for i in num:
					if(i=="0" or i=="1" or i=="2" or i=="3" or i=="4"
						or i=="5" or i=="6" or i=="7" or i=="8"):
						check=True
					else:
						check=False

				if check==False:
					# print en salida valor incorrecto
					textoEntradaBin.place(x=1000, y=213)
					textoEntradaHex.place(x=1000, y=213)
					textoEntradaDec.place(x=1000, y=212)
					textoEntradaOct.place(x=25, y=213)
					entradaNumero.delete(0, END)
					break;
				if check==True:
					textoEntradaBin.place(x=1000, y=213)
					textoEntradaOct.place(x=1000, y=213)
					textoEntradaHex.place(x=1000, y=213)
					textoEntradaDec.place(x=1000, y=212)
					break;
			octToDec=int(num, 8)
			numDec=int(octToDec)
			entradaNumero.delete(0, END)

		
			


		elif tipoNumero1=="hexadecimal": #hexadecimal
			num=num.upper()
			while(True):
				for i in num:
					if(i=="0" or i=="1" or i=="2" or i=="3" or i=="4"
						or i=="5" or i=="6" or i=="7" or i=="8" or i=="9"
						 or i=="A" or i=="B" or i=="C" or i=="D" or i=="E" or i=="F"):
						check=True
					else:
						check=False

				if check==False:
					# print en salida valor incorrecto
					textoEntradaOct.place(x=1000, y=213)
					textoEntradaBin.place(x=1000, y=213)
					textoEntradaDec.place(x=1000, y=212)
					textoEntradaHex.place(x=25, y=213)
					entradaNumero.delete(0, END)
					break;
				if check==True:
					textoEntradaBin.place(x=1000, y=213)
					textoEntradaOct.place(x=1000, y=213)
					textoEntradaHex.place(x=1000, y=213)
					textoEntradaDec.place(x=1000, y=212)
					break;
			hexToDec=int(num, 16)
			numDec=int(hexToDec)
			entradaNumero.delete(0, END)
			salidaNumero.insert(0, numDec)	







		#OPERAR
		if tipoNumero2=="decimal":
			entradaNumero.delete(0, END)
			salidaNumero.insert(0, numDec)

		elif tipoNumero2=="binaria":
			numToBin=(bin(numDec)[2:])
			entradaNumero.delete(0, END)
			salidaNumero.insert(0, numToBin)

		elif tipoNumero2=="octal":
			numToOct=(oct(numDec)[2:])
			entradaNumero.delete(0, END)
			salidaNumero.insert(0, numToOct)

		elif tipoNumero2=="hexadecimal":
			numToHex=(hex(numDec)[2:])
			entradaNumero.delete(0, END)
			salidaNumero.insert(0, numToHex)
	except:
		pass








#----------------------------------------INTERFAZ------------------------------------------------
#Ventana
ventana=Tk()
ventana.title("Conversor Decimal")
ventana.resizable(0,0)
ventana.geometry("417x500")
ventana['bg'] = '#AEC5CC'


#Encabezado y titulo
encabezado=Label(ventana, width=500, height=4)
encabezado['bg'] = '#333'
encabezado.place(x=0, y=00)
titulo=Label(ventana, text="Conversor de bases", font=("Berlin Sans FB", 27), fg="white")
titulo['bg'] = '#333'
titulo.place(x=58, y=10)


#lista desplegable 1
lista_desplegable1=ttk.Combobox(ventana, width=14)
lista_desplegable1.place(x=90, y=97)
lista_desplegable1['values'] = ("decimal", "binaria", "octal", "hexadecimal")
lista_desplegable1.current(0)

textoLista1=Label(ventana, text="De base", font=("Berlin Sans FB", 12), bg="#AEC5CC")
textoLista1.place(x=25, y=97)


#lista desplegable 2
lista_desplegable2=ttk.Combobox(ventana, width=14)
lista_desplegable2.place(x=270, y=97)
lista_desplegable2['values'] = ("decimal", "binaria", "octal", "hexadecimal")
lista_desplegable2.current(1)

textoLista2=Label(ventana, text="a base", font=("Berlin Sans FB", 12), bg="#AEC5CC")
textoLista2.place(x=215, y=97)


#Entrada input
numEntrada=StringVar()
entradaNumero = Entry(ventana, textvariable=numEntrada, font="Arial 14", width=32)
entradaNumero.place(x=27, y=180)

textoEntrada=Label(ventana, text="Introduce aquí el número que quieres convertir", font=("Berlin Sans FB", 12), bg="#AEC5CC")
textoEntrada.place(x=25, y=150)


#Boton submit
btnSubmit = Button(ventana, text = "Convertir", width = 8, height = 2, command = submitDatos)
btnSubmit.place(x=318, y=240)


#Salida resultado
salidaNumero = Entry(ventana, font="Arial 14", width=32)
salidaNumero.place(x=27, y=410)

textoSalida=Label(ventana, text="Número convertido", font=("Berlin Sans FB", 12), bg="#AEC5CC")
textoSalida.place(x=25, y=385)


#Footer
footer=Label(ventana, font=("Berlin Sans FB", 20), width=500, height=500)
footer['bg'] = '#333'
footer.place(x=-500, y=460)

texto_footer=Label(ventana, text="Developed by apalacios.dev", font=("Berlin Sans FB", 12), fg="white")
texto_footer['bg'] = '#333'
texto_footer.place(x=110, y=468)





#MENSAJES DE ERROR    (posicion natural -> "x=25,y=213")
#Mensaje error binario
textoEntradaDec=Label(ventana, text="*Asegurate que tu número cumpla con los carácteres decimales", font=("Berlin Sans FB", 10), bg="#AEC5CC", fg="red")
textoEntradaDec.place(x=-1000, y=213)

#Mensaje error binario
textoEntradaBin=Label(ventana, text="*Asegurate que tu número cumpla con los carácteres binarios", font=("Berlin Sans FB", 10), bg="#AEC5CC", fg="red")
textoEntradaBin.place(x=-1000, y=213)

#Mensaje error octal
textoEntradaOct=Label(ventana, text="*Asegurate que tu número cumpla con los carácteres octales", font=("Berlin Sans FB", 10), bg="#AEC5CC", fg="red")
textoEntradaOct.place(x=-1000, y=213)

#Mensaje error hexadecimal
textoEntradaHex=Label(ventana, text="*Asegurate que tu número cumpla con los carácteres hexadecimales", font=("Berlin Sans FB", 10), bg="#AEC5CC", fg="red")
textoEntradaHex.place(x=-1000, y=213)



ventana.mainloop()