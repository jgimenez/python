# coding:utf-8

from flask import Flask
from flask import request

app = Flask(__name__)

@app.route('/')
def index():
    return 'Hello, World!'

# Parámetros en formato antiguo
# 
# http://127.0.0.1:5000/alumno/datos?nombre=Joni
# http://127.0.0.1:5000/alumno/datos?edad=2000
# http://127.0.0.1:5000/alumno/datos?nombre=Joni&edad=2000
# Los parámetro pueden ser opcionales

@app.route('/alumno/datos')
def joni():
    nombre=request.args.get('nombre','Nombre por defecto')
    edad=request.args.get('edad','Años por defecto')
    return 'Hola ' + nombre + ' tienes ' + edad + ' años'


if __name__ == '__main__':
    app.run(debug=True, port=5000)
