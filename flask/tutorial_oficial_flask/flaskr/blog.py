from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for
)
from werkzeug.exceptions import abort

from flaskr.auth import login_required
from flaskr.db import get_db

bp = Blueprint('blog', __name__)

"""
Cargamos todos los posts
"""
@bp.route('/')
def index():
    db = get_db()
    posts = db.execute(
        'SELECT p.id, title, body, created, author_id, username'
        ' FROM post p JOIN user u ON p.author_id = u.id'
        ' ORDER BY created DESC'
    ).fetchall()
    return render_template('blog/index.html', posts=posts)


"""
Para crear un post.
Comprueba que el usario esté loginado, si no lo está lo envia a la url de login /auth/login
Si no hay errores lo inserta en la base de datos
"""
@bp.route('/create', methods=('GET', 'POST'))
@login_required
def create():
    if request.method == 'POST':
        title = request.form['title']
        body = request.form['body']
        error = None

        if not title:
            error = 'Title is required.'

        if error is not None:
            flash(error)
        else:
            db = get_db()
            db.execute(
                'INSERT INTO post (title, body, author_id)'
                ' VALUES (?, ?, ?)',
                (title, body, g.user['id'])
            )
            db.commit()
            return redirect(url_for('blog.index'))

    return render_template('blog/create.html')


"""
Para obtener un post.
Usado por 'update' y 'delete' (para evitar código duplicado)
Por defecto sólo devuelve el post si el del usario actual (loginado)  check_author=True
Pero podemos obtener el post de cualquier usuario
    get_post(id, False)

abort() lanza una excepción especial que devuelve el código de estado HTTP.
    404 significa “Not Found” -> Si no encuentra el post
    403 significa “Forbidden” -> Si lo encuentra pero no es del usuario loginado
    401 significa “Unauthorized”, pero no lo usamos ya que nos redirecciona a la
        página de login en vez de delvolver ese estado (el usuario no ve la página 401)
"""
def get_post(id, check_author=True):
    post = get_db().execute(
        'SELECT p.id, title, body, created, author_id, username'
        ' FROM post p JOIN user u ON p.author_id = u.id'
        ' WHERE p.id = ?',
        (id,)
    ).fetchone()

    if post is None:
        abort(404, "Post id {0} doesn't exist.".format(id))

    if check_author and post['author_id'] != g.user['id']:
        abort(403)

    return post


"""
Para modificar un post.
El 'id' del post se pasa por la url
Comprueba que el usario esté loginado, si no lo está lo envia a la url de login /auth/login
Si no hay errores lo modifica en la base de datos y lo redirecciona a index.html
"""
@bp.route('/<int:id>/update', methods=('GET', 'POST'))
@login_required
def update(id):
    post = get_post(id)

    if request.method == 'POST':
        title = request.form['title']
        body = request.form['body']
        error = None

        if not title:
            error = 'Título obligatorio.'

        if error is not None:
            flash(error)
        else:
            db = get_db()
            db.execute(
                'UPDATE post SET title = ?, body = ?'
                ' WHERE id = ?',
                (title, body, id)
            )
            db.commit()
            return redirect(url_for('blog.index'))

    return render_template('blog/update.html', post=post)


"""
Para eliminar un post.
El 'id' del post se pasa por la url
Comprueba que el usario esté loginado, si no lo está lo envia a la url de login /auth/login
Si no hay errores lo borrad de la base de datos y lo redirecciona a index.html
No tiene un template html propio. Forma parte de 'update'. Es un botón en ese formulario.
"""
@bp.route('/<int:id>/delete', methods=('POST',))
@login_required
def delete(id):
    get_post(id)
    db = get_db()
    db.execute('DELETE FROM post WHERE id = ?', (id,))
    db.commit()
    return redirect(url_for('blog.index'))