-- Descargar este archivo a /tmp
-- # psql
-- postgres=# \i /tmp/bd.sql

SET statement_timeout= 0;
SET client_encoding= 'UTF8';
SET standard_conforming_strings= off;
SET check_function_bodies= false;
SET client_min_messages= warning;
SET escape_string_warning= off;
SET search_path= public, pg_catalog;
SET default_tablespace= '';
SET default_with_oids= false;

DROP DATABASE IF EXISTS tienda;
CREATE DATABASE tienda;

\c tienda

DROP TABLE IF EXISTS clientes;
DROP TABLE IF EXISTS empleados;



CREATE TABLE empleados (
    id SERIAL, --Clave primaria--
    nombre CHARACTER(20) NOT NULL,
    numero_telefono BIGINT,
    PRIMARY KEY (id)
);

ALTER TABLE public.empleados OWNER TO postgres;




CREATE TABLE clientes (
    id SERIAL, --Clau primaria--
    id_empleado INTEGER NOT NULL, --Clau forania--
    nombre CHARACTER(20) NOT NULL,
    apellidos CHARACTER(20) NOT NULL,
    fecha_nacimiento DATE NOT NULL,
    primary key (id),
    foreign key (id_empleado) REFERENCES empleados(id)
);

ALTER TABLE public.clientes OWNER TO postgres;





INSERT INTO empleados (nombre,numero_telefono) VALUES
    ('Adrià',697589665),
    ('Noelia',658883365),
    ('Pol',776852555),
    ('Emily',611211233),
    ('Alex',677852658),
    ('Maria',677852658),
    ('Joel',698569875)
;


INSERT INTO clientes (id_empleado,nombre,apellidos,fecha_nacimiento) VALUES
    (1,'Rosa','Martinez','1990-05-12'),
    (1,'Arnau','Lloveras','1995-04-11'),
    (2,'Eric','Sánchez','1969-11-01'),
    (3,'Pau','Casals','1979-08-29'),
    (6,'Cristina','Gomez','2000-10-03'),
    (7,'Samuel','Ruiz','2001-09-05'),
    (5,'Joni','Gimenez','1990-08-19'),
    (5,'Bart','Simpson','1980-08-21'),
    (5,'Lisa','Simpson','1980-01-06'),
    (1,'Clark','Kent','1970-02-19'),
    (1,'Mani','Delgado','2000-09-18'),
    (3,'Sheldon','Cooper','1973-05-11'),
    (6,'Barry','Goldberg','1999-11-08'),
    (6,'Homer','Simpson','1960-12-09')
;
