# coding:utf-8
"""
Página web multipestaña:
    home
    mostrar tabla -> Muestra cualquier tabla de la BD potsgres seleccionada
    about
    contact
"""

########################################################################
#                               IMPORT
########################################################################
from flask import Flask, flash, get_flashed_messages, session, redirect, render_template, \
     request, url_for
# Para mostrar errores con flash y bootstrap tenemos los tipos:
#   success, info, warning, danger 

from datetime import datetime
import re
import psycopg2

# Para poder en python2 mostrar campos de la BD de tipo utf8 (ñ,ç,à,á,...)
# En python3 hay que comentar las 4 líneas
"""
import sys
if sys.version_info.major < 3:
    reload(sys)
sys.setdefaultencoding('utf8')
"""


########################################################################
#                   VARIABLES GLOBALES Y CONSTANTES
########################################################################



########################################################################
#                               NIVEL 4
########################################################################



########################################################################
#                               NIVEL 3
########################################################################



########################################################################
#                               NIVEL 2
########################################################################
def control_errores_es_entero(variable_string):
    """
    Comprueba si el string pasado como parámetro es entero

    Returns:
        Boolean -- Si es entero->True
    """

    try:
        int(variable_string)
        es_entero = True
    except ValueError:
        es_entero = False

    return es_entero

def comprobar_errores_empleados(nombre,numero_telefono):
    hay_error = False
    
    if not nombre:
        error = 'El nombre es obligatorio'
        flash(error,'danger')
        hay_error = True
    else:
        if len(nombre) >20:
            error = 'La longitud máxima del nombre son 20 caracteres'
            flash(error,'danger')
            hay_error = True

    if numero_telefono and not numero_telefono.isnumeric():
        error = 'El número de teléfono debe ser numérico'
        flash(error,'danger')
        hay_error = True

    return hay_error



########################################################################
#                               NIVEL 1 (MAIN)
########################################################################
app = Flask(__name__)

# "secretkey" es necesario para usar flash() para los errores
# Lo hace mdiante la sesión, que se hace con una cookie
# firmada con esta clave para evitar que manipulen su contenido
app.secret_key = 'many random bytes'

########################################################################
@app.route("/")
def home():
    return render_template("home.html")

########################################################################
@app.route("/about/")
def about():
    return render_template("about.html")

########################################################################
@app.route("/contact/")
def contact():
    return render_template("contact.html")

########################################################################
@app.route("/api/data/")
def get_data():
    #return app.send_static_file("data.json")
    return app.send_static_file("json/data.json")

########################################################################
@app.route("/mostrar_tabla_bd/", methods=('GET', 'POST'))
def mostrar_tabla_bd():
    # La primera vez que se carga la página del formulario lo hace con GET
    # También nos pueden llamar con GET y la variable de 
    # sesión 'nombre_tabla' informada después de modificar o borrar un registro
    if request.method == 'GET' and not session.get('nombre_tabla'):
        return render_template("mostrar_tabla_bd.html",tot_rows=0)

    # Cuando el usuario ya ha entrado en el formulario y lo ha enviado con
    # el botón "Submit" se hace con POST
    elif request.method == 'POST' or (request.method == 'GET' and session.get('nombre_tabla')) :
        # Nos llaman con GET y la variable de sesión 'nombre_tabla' informada
        # después de modificar o borrar un registro
        if request.method == 'GET' and session.get('nombre_tabla'):
            # Al usar 'pop' en lugar de 'get' , leemos la variable y luego la borramos
            nombre_tabla = session.pop('nombre_tabla')
        else:
            # Es un POST normal
            # Leemos los valores introducidos en el formulario
            nombre_tabla = request.form['nombre_tabla']

        # Si han dejado en blanco el campo "nombre_tabla"
        if not nombre_tabla:
            error = 'El nombre de la tabla es obligatorio'
            flash(error,'danger')
            return render_template("mostrar_tabla_bd.html",tot_rows=0)
        else:
            try:
                conn = psycopg2.connect(database="tienda", user="postgres", password="jupiter")
                print("DATABASE OPENED SUCCESSFULLY \n")

            except:
                print("CONNECTION ERROR")
                exit(2)

            cur = conn.cursor()

            try:
                sql=" SELECT * " + \
                    " FROM " + nombre_tabla + \
                    " ORDER BY id "
                cur.execute(sql)

                rows = cur.fetchall() # Obtenemos todas las filas
                tot_rows = rows.__len__() # Obtenemos el número total de filas
                tot_columns = rows[0].__len__() # Obtenemos el número total de columnas
                list_columns = [columnames[0] for columnames in cur.description] # Obtenemos las cabecceras

            except psycopg2.Error as er :
                # Error=42P01   ->   No existe la tabla en la BD
                if (er.pgcode=="42P01"):
                    error = 'La tabla no existe en la BD'
                    flash(error,'danger')
                    return render_template("mostrar_tabla_bd.html",tot_rows=0)
                else:
                    print("-------- ERROR:", er.pgcode, " -------- \n")
                    exit(2)
                conn.rollback()

            conn.close()

            return render_template("mostrar_tabla_bd.html",
                                tot_rows=tot_rows,rows=rows,
                                tot_columns=tot_columns,list_columns=list_columns,
                                nombre_tabla=nombre_tabla)

########################################################################
@app.route("/anyadir_empleados/", methods=('GET', 'POST'))
def anyadir_empleados():
    # La primera vez que se carga la página del formulario lo hace con GET
    if request.method == 'GET':
        return render_template("anyadir_empleados.html")

    # Cuando el usuario ya ha entrado en el formulario y lo ha enviado con
    # el botón "Submit" se hace con POST
    elif request.method == 'POST':
        # Leemos los valores introducidos en el formulario
        nombre = request.form['nombre']
        numero_telefono = request.form['numero_telefono']

        # Inicializamos el error (por defecto no hay error)
        hay_error = False

        hay_error = comprobar_errores_empleados(nombre,numero_telefono)

        if hay_error:
            return render_template("anyadir_empleados.html",
                                    nombre=nombre,
                                    numero_telefono=numero_telefono)
        else:    # No hay errores
            try:
                conn = psycopg2.connect(database="tienda", user="postgres", password="jupiter")
                print("DATABASE OPENED SUCCESSFULLY \n")

            except:
                print("CONNECTION ERROR")
                exit(2)

            cur = conn.cursor()

            try:
                if not numero_telefono:
                    numero_telefono = "NULL"
                sql=" INSERT INTO empleados (nombre,numero_telefono) VALUES " + \
                    "  ('" + str(nombre) + "' , " + \
                    "    " + str(numero_telefono) + " ) "
                cur.execute(sql)
                conn.commit()

            except psycopg2.Error as er :
                print("DATABASE ERROR")
                exit(2)
                conn.rollback()

            conn.close()

            mensaje = 'La creación se ha realizado con éxito'
            flash(mensaje,'success')
            
            session['nombre_tabla'] = "empleados"
            url = "/mostrar_tabla_bd/"
            return redirect(url)

########################################################################
@app.route("/api/modificar_registro_tabla/<nombre_tabla>/<int:id>/")
def modificar_registro_tabla(nombre_tabla,id):
    # Inicializamos el error (por defecto no hay error)
    error = None

    try:
        conn = psycopg2.connect(database="tienda", user="postgres", password="jupiter")
        print("DATABASE OPENED SUCCESSFULLY \n")

    except:
        print("CONNECTION ERROR")
        exit(2)

    cur = conn.cursor()

    try:
        sql=" SELECT * FROM " + nombre_tabla + \
            " WHERE id=" + str(id)
        cur.execute(sql)

        rows = cur.fetchall() # Obtenemos todas las filas


        if (nombre_tabla == "empleados"):
            nombre          = rows[0][1]
            numero_telefono = rows[0][2]    
            url="/modificar_empleados/" + str(id) + "/" + str(nombre) + "/" + str(numero_telefono)

        if (nombre_tabla == "clientes"):
            id_empleado      = rows[0][1]
            nombre           = rows[0][2]
            apellidos        = rows[0][3]
            fecha_nacimiento = rows[0][4]
            url="/modificar_clientes/" + str(id) + "/" + str(id_empleado) + "/" + str(nombre) +   \
                                   "/" + str(apellidos) + "/" + str(fecha_nacimiento)

        return redirect(url)

    except psycopg2.Error as er :
        print("DATABASE ERROR")
        exit(2)
        conn.rollback()

    conn.close()

########################################################################
@app.route("/modificar_empleados/<int:id>/<nombre>/<numero_telefono>/", methods=('GET', 'POST'))
def modificar_empleados(id,nombre,numero_telefono):
    # La primera vez que se carga la página del formulario lo hace con GET
    if request.method == 'GET':

        # Si el campo numero_telefono (no obligatorio) en la tabla de la BD es NULL
        # se carga en la tabla html con valor "None"
        # Para el formulario, significa que está en blanco
        if numero_telefono == "None":
            numero_telefono = ""

        return render_template("modificar_empleados.html",
                                id=id,
                                nombre=nombre,
                                numero_telefono=numero_telefono)

    # Cuando el usuario ya ha entrado en el formulario y lo ha enviado con
    # el botón "Submit" se hace con POST
    elif request.method == 'POST':
        # Leemos los valores introducidos en el formulario
        nombre = request.form['nombre']
        numero_telefono = request.form['numero_telefono']

        # Inicializamos el error (por defecto no hay error)
        hay_error = False

        hay_error = comprobar_errores_empleados(nombre,numero_telefono)

        if hay_error:
            return render_template("modificar_empleados.html",
                                    id=id,
                                    nombre=nombre,
                                    numero_telefono=numero_telefono)
        else:    # No hay errores
            try:
                conn = psycopg2.connect(database="tienda", user="postgres", password="jupiter")
                print("DATABASE OPENED SUCCESSFULLY \n")

            except:
                print("CONNECTION ERROR")
                exit(2)

            cur = conn.cursor()

            try:
                if not numero_telefono:
                    numero_telefono = "NULL"
                sql=" UPDATE empleados " + \
                    " SET nombre = '" + str(nombre) + "' , " + \
                    "     numero_telefono = " + str(numero_telefono) + \
                    " WHERE id=" + str(id)
                cur.execute(sql)
                conn.commit()

            except psycopg2.Error as er :
                print("DATABASE ERROR")
                exit(2)
                conn.rollback()

            conn.close()

            mensaje = 'La modificación se ha realizado con éxito'
            flash(mensaje,'success')
            
            session['nombre_tabla'] = "empleados"
            url = "/mostrar_tabla_bd/"
            return redirect(url)

########################################################################
@app.route("/api/borrar_registro_tabla/<nombre_tabla>/<int:id>/")
def borrar_registro_tabla(nombre_tabla,id):
    # Inicializamos el error (por defecto no hay error)
    error = None

    try:
        conn = psycopg2.connect(database="tienda", user="postgres", password="jupiter")
        print("DATABASE OPENED SUCCESSFULLY \n")

    except:
        print("CONNECTION ERROR")
        exit(2)

    cur = conn.cursor()

    try:
        sql=" SELECT * FROM " + nombre_tabla + \
            " WHERE id=" + str(id)
        cur.execute(sql)

        rows = cur.fetchall() # Obtenemos todas las filas


        if (nombre_tabla == "empleados"):
            nombre          = rows[0][1]
            numero_telefono = rows[0][2]    
            url="/borrar_empleados/" + str(id) + "/" + str(nombre) + "/" + str(numero_telefono)

        if (nombre_tabla == "clientes"):
            id_empleado      = rows[0][1]
            nombre           = rows[0][2]
            apellidos        = rows[0][3]
            fecha_nacimiento = rows[0][4]
            url="/borrar_clientes/" + str(id) + "/" + str(id_empleado) + "/" + str(nombre) +   \
                                "/" + str(apellidos) + "/" + str(fecha_nacimiento)

        return redirect(url)

    except psycopg2.Error as er :
        print("DATABASE ERROR")
        exit(2)
        conn.rollback()

    conn.close()

########################################################################
@app.route("/borrar_empleados/<int:id>/<nombre>/<numero_telefono>/", methods=('GET', 'POST'))
def borrar_empleados(id,nombre,numero_telefono):
    # La primera vez que se carga la página del formulario lo hace con GET
    if request.method == 'GET':
        return render_template("borrar_empleados.html",
                                id=id,
                                nombre=nombre,
                                numero_telefono=numero_telefono)

    # Cuando el usuario ya ha entrado en el formulario y lo ha enviado con
    # el botón "Submit" se hace con POST
    elif request.method == 'POST':
        try:
            conn = psycopg2.connect(database="tienda", user="postgres", password="jupiter")
            print("DATABASE OPENED SUCCESSFULLY \n")

        except:
            print("CONNECTION ERROR")
            exit(2)

        cur = conn.cursor()

        try:
            sql=" DELETE FROM empleados " + \
                " WHERE id=" + str(id)
            cur.execute(sql)
            conn.commit()

        except psycopg2.Error as er :
            # Error=23503   ->   Violación de integridad por clave foránea
            if (er.pgcode=="23503"):
                error = 'No se puede borrar un empleado con clientes asignados. Primero borre todos los clientes asignados y luego el empleado.'
                flash(error,'danger')

                session['nombre_tabla'] = "empleados"
                url = "/mostrar_tabla_bd/"
                return redirect(url)
            else:
                print("-------- ERROR:", er.pgcode, " -------- \n")
                exit(2)
            conn.rollback()

        conn.close()

        mensaje = 'El borrado se ha realizado con éxito'
        flash(mensaje,'success')
        
        session['nombre_tabla'] = "empleados"
        url = "/mostrar_tabla_bd/"
        return redirect(url)

########################################################################
if __name__ == "__main__":
    app.run(debug=True, port=5000)