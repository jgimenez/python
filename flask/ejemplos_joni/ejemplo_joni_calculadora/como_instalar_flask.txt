Crear directorio
mkdir flask
cd flask/

Activar entorno virtual (cada vez que reinicies hay que hacer estos pasos)
sudo apt-get install python3-venv
python3 -m venv venv
. venv/bin/activate

Instalar flask dentro del entorno virtual
pip install --upgrade pip
pip install Flask

Ejecutar el servidor:
    Cada vez desde terminal
        export FLASK_APP=app.py
        export FLASK_ENV=development
        flask run

    O bien, configurar .flaskenv
        Creamos el archivo .flaskenv
        Añadimos las líneas
            FLASK_APP=app.py
            FLASK_ENV=development
        Para ejecutarlo
        flask run
    O bien
        python3 -m flask  run





Comprobar en el navegador: http://localhost:5000
