# coding:utf-8
"""
Poner y quitar acceso a Internet en un aula
Activando/desactivando las reglas de NAT de un router mikrotik
"""
from subprocess import check_output, call
from flask import Flask, render_template, request, url_for
app = Flask(__name__)

@app.route("/", methods=("GET", "POST"))
def index():
    dir_ip_mikrotik="192.168.100.40"

    # Comprobamos si los alumnos tienen acceso a Internet
    estado_binario = check_output('ssh admin@'+dir_ip_mikrotik+' ip firewall nat print count-only  where disabled', shell=True)
    estado=estado_binario.decode('ascii')[0]
    
    if estado == "0":
        acceso_internet="SI"
        poner_quitar="Quitar acceso a Internet"
    else:
        acceso_internet="NO"
        poner_quitar="Poner acceso a Internet"

    # La primera vez que se carga la página del formulario lo hace con GET
    if request.method == "GET":
        return render_template("index.html",
                               acceso_internet=acceso_internet,
                               poner_quitar=poner_quitar)

    # Cuando el usuario ya ha entrado en el formulario y lo ha enviado con
    # el botón "Submit" se hace con POST
    elif request.method == "POST":
        if estado == "0":
            call('ssh admin@'+dir_ip_mikrotik+' ip firewall nat disable numbers=0', shell=True)
            acceso_internet="NO"
            poner_quitar="Poner acceso a Internet"
        else:
            call('ssh admin@'+dir_ip_mikrotik+' ip firewall nat enable numbers=0', shell=True)
            acceso_internet="SI"
            poner_quitar="Quitar acceso a Internet"

        return render_template("index.html",
                               acceso_internet=acceso_internet,
                               poner_quitar=poner_quitar)

if __name__ == "__main__":
    app.run(debug=True, port=5000)