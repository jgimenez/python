# coding:utf-8
"""
Página web que suma dos números enteros (positivos o negativos)
No hace control de errores!
"""
from flask import Flask, render_template, request, url_for
app = Flask(__name__)

@app.route("/sumar/", methods=('GET', 'POST'))
def sumar():
    # La primera vez que se carga la página del formulario lo hace con GET
    if request.method == 'GET':
        resultado=0
        return render_template("sumar.html",resultado=resultado)

    # Cuando el usuario ya ha entrado en el formulario y lo ha enviado con
    # el botón "Submit" se hace con POST
    elif request.method == 'POST':
        # Leemos los valores introducidos en el formulario
        operando1 = request.form['operando1']
        operando2 = request.form['operando2']

        # Inicializamos el resultado
        resultado = 0
        resultado = int(operando1) + int(operando2)

        return render_template("sumar.html",resultado=resultado)

if __name__ == "__main__":
    app.run(debug=True, port=5000)